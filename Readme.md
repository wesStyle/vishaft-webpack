# Валопровод-К
> Интерфейс Валопровода на JavaScript

[![NPM Version][npm-image]][npm-url]

Web-интерфейс приложения с использованием современного стека JavaScript.

* Для сборки - webpack 4: https://webpack.js.org/
* Для компонентов интерфейса - Riot.js: http://riotjs.com/
* Для транспайлинга ES6 - Babel: https://babeljs.io/


## Установка

```
npm install
npm run prod
```

После этого в директории /dist/ будет находится собранное приложение и все его ресурсы.

## Зависимости

```
"@fortawesome/fontawesome": "^1.1.5",
"@fortawesome/fontawesome-free-brands": "^5.0.10",
"@fortawesome/fontawesome-free-regular": "^5.0.10",
"@fortawesome/fontawesome-free-solid": "^5.0.10",
"bootstrap": "^4.1.0",
"bootstrap-colorpicker": "^2.5.2",
"bootstrap-treeview": "^1.2.0",
"jquery": "^3.3.1",
"popper.js": "^1.14.3",
"riot": "^3.9.1",
"riot-hot-reload": "^1.0.0",
"sugar": "^2.0.4",
"three": "^0.91.0",
"three-csg": "^1.0.0",
"validate.js": "^0.12.0"
```

## Доступные команды

Сборка без оптимизаций

```
npm run build
```

Сборка с оптимизациями

```
npm run prod
```

Запуск в dev-режиме (с поддержкой HMR) на порту 8080

```
npm run start
```

Запуск в режиме watch на порту 8080

```
npm run watch
```

Сборка и графический анализ полученных chunk-ов

```
npm run analyze
```

Прогон всех исходных файлов через eslint

```
npm run lint
```

<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics