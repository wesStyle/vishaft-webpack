<app>
  <nav class="navbar navbar-dark py-0 bg-primary navbar-expand-lg py-md-0">
    <div class="logo"></div>
    <a class="navbar-brand" href="#">Валопровод-К</a>
    <button class="navbar-toggler mt-1" type="button" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fa fa-bars"></i>
    </button>
    <div class="navbar-collapse collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item py-0">
          <a id="new_project" href="#" class="nav-link">
            <i class="fas fa-file fa-fw"></i> Новый проект</a>
        </li>
        <!-- <li class="nav-item py-0">
          <a id="open_project" href="#" class="nav-link">
            <i class="fas fa-folder-open fa-fw"></i> Открыть проект</a>
        </li>
        <li class="nav-item py-0">
          <a id="save_project" href="#" class="nav-link">
            <i class="fas fa-save fa-fw"></i> Сохранить проект</a>
        </li> -->
      </ul>
    </div>
  </nav>
  <div class="container-fluid h-100" id="main-container">
    <div class="row justify-content-center h-100">
      <div class="col-2" id="sidebar">
        <div id="sidebar-top">
          <add-element></add-element>
          <br />
          <div id="tree-slot">
            <tree></tree>
          </div>
        </div>
      </div>
      <div class="col-3" id="prop-col">
        <prop></prop>
      </div>
      <div class="col-7" id="rndr-area">
        <div id="rndr-toolbar"></div>
        <three></three>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" ref="loading_modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
      <div class="modal-content" style="width: 48px">
        <span class="fa fa-spinner fa-spin fa-3x"></span>
      </div>
    </div>
  </div>

  <script>
    import './tags/tree.tag';
    import './tags/prop.tag';
    import './tags/add-element.tag';
    import './tags/three/three.tag';

    const tag = this;

    class Shaft {
      constructor() {
        this.parts = [];
        this.text = 'Новый проект';
      }
    }

    function newProject() {
      const shaft = new Shaft();
      riot.shaft = shaft;
      riot.observable(shaft);

      riot.shaft.on('loading-finished', () => {
        console.log('loading finished');
        $(tag.refs.loading_modal).modal('hide');
      });
    }

    newProject();
    tag.on('mount', () => {
      console.log('loading...');
      $(tag.refs.loading_modal).modal('show');

      $('#new_project').click(() => {
        riot.shaft.parts = [];
        riot.shaft.trigger('part-changed');
        riot.shaft.selected = undefined;
        riot.shaft.trigger('selection-changed');
      });

      $('#save_project').click(() => {
        const data = JSON.stringify(riot.shaft);
        $.ajax({
          type: 'POST',
          contentType: 'application/json',
          url: '/save',
          data,
          timeout: 100000,
          success(data1) {
            console.log('SUCCESS: ', data1);
          },
          error(e) {
            console.log('ERROR: ', e);
          },
          done(e) {
            console.log('DONE', e);
          },
        });
      });
    });
  </script>
</app>