<!--
Рендер Валопровода

TODO: Большой рефакторинг
-->

<three>
  <div id="render">
    <rndr-toolbar />
    <div id="inset">
    </div>
  </div>

  <script>
    import 'three/examples/js/controls/OrbitControls';
    import 'three/examples/js/loaders/OBJLoader';
    import ThreeBSP from 'three-csg';

    import './rndr-toolbar.tag';

    const tag = this;

    tag.on('mount', () => {
      let camera;
      let controls;
      let light;
      let scene;
      let renderer;
      let valGroup;
      const container = document.getElementById('render');
      let raycaster;
      let mouse;
      let arrow;

      const strDownloadMime = 'image/octet-stream';

      const allPromises = [];
      let gear;
      let propeller;

      let lastSelected;

      let scene2;
      let camera2;
      let renderer2;
      let elementOffset = 0;

      function init() {
        scene = new THREE.Scene();
        scene.background = new THREE.Color(0xFFFFFF);

        valGroup = new THREE.Group();
        scene.add(valGroup);

        // camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
        const scale = 20;
        camera = new THREE.OrthographicCamera(
          $(container).width() / -scale, $(container).width() / scale,
          $(container).height() / scale, $(container).height() / -scale,
          1,
          10000,
        );

        camera.position.z = 100;
        camera.position.x = 100;
        camera.position.y = 100;
        scene.add(camera);

        light = new THREE.DirectionalLight(0xFFFFFF);
        // light.position.set(100, 100, 100);
        scene.add(light);

        // const helper = new THREE.DirectionalLightHelper(light, 50);
        // scene.add(helper);

        const plane = new THREE.GridHelper(100, 100);
        plane.material.transparent = true;
        plane.material.opacity = 0.2;
        scene.add(plane);
        const worldAxis = new THREE.AxesHelper(1000);
        scene.add(worldAxis);

        renderer = new THREE.WebGLRenderer({
          // antialias: true,
          preserveDrawingBuffer: true,
        });


        renderer.setSize($(container).width(), $(container).height());
        container.appendChild(renderer.domElement);

        controls = new THREE.OrbitControls(camera, renderer.domElement);

        controls.enableKeys = false;

        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();
      }

      function miniAxis() {
        const container2 = document.getElementById('inset');

        renderer2 = new THREE.WebGLRenderer({
          alpha: true,
          // antialias: true
        });
        // renderer2.setClearColor( 0xf0f0f0, 1 );
        renderer2.setSize(150, 150);
        container2.appendChild(renderer2.domElement);

        scene2 = new THREE.Scene();

        // camera2 = new THREE.PerspectiveCamera( 50, 150 / 150, 1, 1000 );
        const scale = 10;
        camera2 = new THREE.OrthographicCamera(
          150 / -scale,
          150 / scale,
          150 / scale,
          150 / -scale,
          1,
          10000,
        );
        camera2.up = camera.up;

        const axes2 = new THREE.AxisHelper(50);
        scene2.add(axes2);
      }

      function getFlange(d1, l1, d2, l2, d3, l3, d4, l4) {
        const material = new THREE.MeshLambertMaterial({
          color: 0x361108,
        });

        const geometry = new THREE.CylinderGeometry(d1, d1, l1, 32);
        const cylinder = new THREE.Mesh(geometry, material);

        const geometry1 = new THREE.CylinderGeometry(d2, d2, l2, 32);
        const cylinder1 = new THREE.Mesh(geometry1, material);
        cylinder1.position.y -= l1;

        const geometry2 = new THREE.CylinderGeometry(d3, d3, l3, 32);
        const cylinder2 = new THREE.Mesh(geometry2, material);
        cylinder2.position.y -= l2;

        const geometry3 = new THREE.CylinderGeometry(d4, d4, l4, 32);
        const cylinder3 = new THREE.Mesh(geometry3, material);
        cylinder3.position.y -= l3;

        const tmp = new THREE.Object3D();
        tmp.add(cylinder);
        tmp.add(cylinder1);
        tmp.add(cylinder2);
        tmp.add(cylinder3);

        tmp.rotation.x = Math.PI / 2;
        tmp.rotation.z = Math.PI / 2;
        return tmp;
      }

      function getMuff(innerDBack, innderDFront, d, lengthInner, lengthOuter) {
        const material = new THREE.MeshLambertMaterial({
          color: 0x361108,
        });

        const geometry = new THREE.CylinderGeometry(innerDBack, innderDFront, lengthInner, 32);
        const cylinder = new THREE.Mesh(geometry, material);

        const geometry1 = new THREE.CylinderGeometry(d, d, lengthOuter, 32);
        const cylinder1 = new THREE.Mesh(geometry1, material);

        const cylinderBsp = new ThreeBSP(cylinder);
        const cylinder1Bsp = new ThreeBSP(cylinder1);
        const valElementBsp = cylinder1Bsp.subtract(cylinderBsp);

        const meshEl = valElementBsp.toMesh(material);
        meshEl.rotation.x = Math.PI / 2;
        meshEl.rotation.z = Math.PI / 2;

        const grp = new THREE.Object3D();
        cylinder.rotation.x = Math.PI / 2;
        cylinder.rotation.z = Math.PI / 2;
        grp.add(cylinder);
        grp.add(meshEl);
        return grp;
      }

      function getHalfCylinder(elemD, d, length, angle1, angle2, materialColor) {
        const material = new THREE.MeshBasicMaterial();

        const geometry = new THREE.CylinderGeometry(elemD + d, elemD + d, length, 32);
        const cylinder = new THREE.Mesh(geometry, material);

        const geometry1 = new THREE.CylinderGeometry(elemD, elemD, length, 32);
        const cylinder1 = new THREE.Mesh(geometry1, material);

        const cylinderBsp = new ThreeBSP(cylinder);
        const cylinder1Bsp = new ThreeBSP(cylinder1);
        const cylHoleBsp = cylinderBsp.subtract(cylinder1Bsp);

        const geometry2 = new THREE.BoxGeometry((elemD + d) * 2, length, (elemD + d) * 2);
        const geometry3 = new THREE.BoxGeometry((elemD + d) * 2, length, (elemD + d) * 2);

        const box = new THREE.Mesh(geometry2, material);
        box.geometry.translate(elemD + d, 0, elemD + d);
        const box1 = new THREE.Mesh(geometry3, material);
        box1.geometry.translate(elemD + d, 0, elemD + d);

        box.rotation.y = angle1;
        box1.rotation.y = angle2;

        const boxBsp = new ThreeBSP(box);
        const box1Bsp = new ThreeBSP(box1);
        const prsmBsp = boxBsp.union(box1Bsp);

        const tmp = cylHoleBsp.subtract(prsmBsp);

        const meshMaterial = new THREE.MeshLambertMaterial({
          color: materialColor,
        });
        const tmpMesh = tmp.toMesh(meshMaterial);
        tmpMesh.userData = { orig_material: meshMaterial };
        return tmpMesh;
      }

      function getGear(d, size) {
        const gearNew = gear.clone();
        gearNew.scale.x += d;
        gearNew.scale.z += size;
        gearNew.scale.y += size;
        return gearNew;
      }

      function getSimpleGear() {
        const gearNew = gear.clone();
        return gearNew;
      }

      function getPropeller(d, size) {
        const propellerNew = propeller.clone();
        propellerNew.scale.x += d;
        propellerNew.scale.z += size;
        propellerNew.scale.y += size;
        return propellerNew;
      }

      function getValElement(innerDBack, innerDFront, outerDBack, outerDFront, length, color) {
        const geometry = new THREE.CylinderGeometry(outerDBack, outerDFront, length, 32);
        const material = new THREE.MeshBasicMaterial();
        const cylinder = new THREE.Mesh(geometry, material);

        const geometry1 = new THREE.CylinderGeometry(innerDBack, innerDFront, length, 32);
        const material1 = new THREE.MeshBasicMaterial();
        const cylinder1 = new THREE.Mesh(geometry1, material1);

        const cylinderBsp = new ThreeBSP(cylinder);
        const cylinder1Bsp = new ThreeBSP(cylinder1);
        const valElementBsp = cylinderBsp.subtract(cylinder1Bsp);

        const meshMaterial = new THREE.MeshLambertMaterial({
          color: parseInt(color.replace(/^#/, ''), 16),
        });

        const meshEl = valElementBsp.toMesh(meshMaterial);

        meshEl.userData = { orig_material: meshMaterial };
        meshEl.rotation.x = Math.PI / 2;
        meshEl.rotation.z = Math.PI / 2;
        return meshEl;
      }

      function getKHat(elemD, d, length, angle1, angle2, materialColor, koffset, innerD, klength) {
        const material = new THREE.MeshBasicMaterial();
        const ring1 = getHalfCylinder(elemD + d, d, length + koffset, angle1, angle2, materialColor);

        const geometry = new THREE.CylinderGeometry(elemD + (2 * d), elemD + d + innerD, klength, 32);
        const cylinder = new THREE.Mesh(geometry, material);

        const geometry1 = new THREE.CylinderGeometry(
          elemD + d + innerD,
          elemD + d + innerD,
          klength,
          32,
        );
        const cylinder1 = new THREE.Mesh(geometry1, material);


        const cylBsp = new ThreeBSP(cylinder);
        const cyl1Bsp = new ThreeBSP(cylinder1);
        const khatBsp = cylBsp.subtract(cyl1Bsp);

        const geometry2 = new THREE.BoxGeometry((elemD + d) * 2, klength, (elemD + d) * 2);
        const geometry3 = new THREE.BoxGeometry((elemD + d) * 2, klength, (elemD + d) * 2);
        const box = new THREE.Mesh(geometry2, material);
        box.geometry.translate(elemD + d, 0, elemD + d);
        const box1 = new THREE.Mesh(geometry3, material);
        box1.geometry.translate(elemD + d, 0, elemD + d);

        box.rotation.y = angle1;
        box1.rotation.y = angle2;

        const boxBsp = new ThreeBSP(box);
        const box1Bsp = new ThreeBSP(box1);
        const prsmBsp = boxBsp.union(box1Bsp);

        const tmp = khatBsp.subtract(prsmBsp);
        const tmpMesh = tmp.toMesh(new THREE.MeshLambertMaterial({
          color: materialColor,
        }));

        ring1.position.x += (length / 2) + (koffset / 2) + (klength / 2);
        ring1.position.y += (length / 2) + (koffset / 2) + (klength / 2);
        tmpMesh.position.x += (length / 2) + (koffset / 2) + (klength / 2);

        const tmpBsp = new ThreeBSP(tmpMesh);
        const ring1Bsp = new ThreeBSP(ring1);
        const resBsp = tmpBsp.union(ring1Bsp);

        const meshMaterial = new THREE.MeshPhongMaterial({
          color: materialColor,
        });

        const mesh = resBsp.toMesh(meshMaterial);
        mesh.userData = { orig_material: meshMaterial };
        return mesh;
      }

      function getBearingK(name, elemD, d, length, koffset, innerD, klength) {
        const bearing2 = new THREE.Object3D();
        const mesh1 = getHalfCylinder(elemD, d, length, Math.PI / 2, Math.PI / 3, 0xf5e470);
        mesh1.rotation.x = Math.PI / 2;
        mesh1.rotation.z = Math.PI / 2;
        mesh1.name = name;
        bearing2.add(mesh1);

        const mesh2 = getKHat(
          elemD,
          d,
          length,
          Math.PI / 2,
          Math.PI / 4,
          0xbdd971,
          koffset,
          innerD,
          klength,
        );
        mesh2.rotation.x = Math.PI / 2;
        mesh2.rotation.z = Math.PI / 2;
        mesh2.name = name;
        bearing2.add(mesh2);
        return bearing2;
      }

      function getBearing(elemD, d, length, koffset) {
        const bearing1 = new THREE.Object3D();
        bearing1.add(getHalfCylinder(
          elemD,
          d,
          length,
          Math.PI / 2,
          Math.PI / 3,
          0xf5e470,
        ));
        bearing1.add(getHalfCylinder(
          elemD + d,
          d,
          length + koffset,
          Math.PI / 2,
          Math.PI / 4,
          0xbdd971,
        ));
        return bearing1;
      }

      function loadModels() {
        allPromises.push(new Promise((resolve, reject) => {
          const loader = new THREE.OBJLoader();
          loader.load(
            'https://dl.dropboxusercontent.com/s/lvijn4qd5irdp24/Gear-main.obj',
            (object) => {
              const material = new THREE.MeshNormalMaterial();
              let mesh;
              object.traverse((child) => {
                console.log('jui');
                console.log(child);
                if (child instanceof THREE.Mesh) {
                  const child1 = child;
                  child1.material = material;
                  mesh = child;
                  console.log('mesh assigned');
                }
              });

              mesh.position.x = -20;
              mesh.position.y = 0;
              mesh.position.z = 0;

              mesh.scale.x -= 0.97;
              mesh.scale.y -= 0.97;
              mesh.scale.z -= 0.97;

              mesh.geometry = new THREE.Geometry().fromBufferGeometry(mesh.geometry);

              const cylinder = new THREE.CylinderGeometry(2, 2, 20, 32);
              const cylinderMaterial = new THREE.MeshBasicMaterial({
                color: 0xf172a1,
              });
              const cylinderMesh = new THREE.Mesh(cylinder, cylinderMaterial);
              cylinderMesh.position.x -= 15;
              cylinderMesh.rotation.x = Math.PI / 2;
              cylinderMesh.rotation.z = Math.PI / 2;

              const cylinderBsp = new ThreeBSP(cylinderMesh);
              const gearBsp = new ThreeBSP(mesh);
              const gearHoleBsp = gearBsp.subtract(cylinderBsp);
              const gearHole = gearHoleBsp.toMesh(new THREE.MeshLambertMaterial({
                color: 0xf172a1,
              }));

              gearHole.scale.x -= 0.97;
              gearHole.scale.y -= 0.97;
              gearHole.scale.z -= 0.97;

              gear = gearHole;
              resolve(gear);
            },
            () => {
              console.log('loaded');
            },
            (error) => {
              console.log('An error happened');
              reject(error);
            },
          );
        }));

        allPromises.push(new Promise((resolve, reject) => {
          const loader1 = new THREE.OBJLoader();
          loader1.load(
            'https://dl.dropboxusercontent.com/s/wewrc7ca17v5qgs/prop-type-2.obj',
            (object) => {
              const material1 = new THREE.MeshLambertMaterial({
                color: 0x97caef,
              });
              let mesh;
              object.traverse((child) => {
                if (child instanceof THREE.Mesh) {
                  const child1 = child;
                  child1.material = material1;
                  mesh = child;
                  console.log('mesh assigned');
                }
              });
              mesh.position.x = 30;
              mesh.position.y = 0;
              mesh.position.z = 0;

              mesh.scale.x -= 0.90;
              mesh.scale.y -= 0.90;
              mesh.scale.z -= 0.90;
              console.log(Object);

              mesh.name = 'obj2';

              propeller = mesh;
              resolve(propeller);
            },
            () => {
              console.log('loaded');
            },
            (error) => {
              console.log('An error happened');
              reject(error);
            },
          );
        }));
      }

      function onClick(event) {
        // mouse.x = ((event.clientX / (window.innerWidth + $(container).offset().left)) * 2) - 1;
        // mouse.y = -((event.clientY / (window.innerHeight)) * 2) + 1;

        const rect = renderer.domElement.getBoundingClientRect();
        mouse.x = (((event.clientX - rect.left) / rect.width) * 2) - 1;
        mouse.y = -(((event.clientY - rect.top) / rect.height) * 2) + 1;

        raycaster.setFromCamera(mouse, camera);

        const intersects = raycaster.intersectObjects(valGroup.children, true);

        scene.remove(arrow);
        arrow = new THREE.ArrowHelper(
          raycaster.ray.direction,
          raycaster.ray.origin,
          100,
          Math.random() * 0xffffff,
        );
        scene.add(arrow);

        if (intersects.length > 0) {
          const intersectObj = intersects[0].object;

          $.each(riot.shaft.parts, (index, value) => {
            if (value.nodes !== undefined) {
              $.each(value.nodes, (index1, value1) => {
                if (value1.id === intersectObj.name) {
                  riot.shaft.selected = value1;
                  riot.shaft.trigger('selection-changed');
                }
              });
            }
            if (value.id === intersectObj.name) {
              riot.shaft.selected = value;
              riot.shaft.trigger('selection-changed');
            }
          });

          console.log(`intersect with ${intersects}`);

          if (lastSelected && intersectObj.id !== lastSelected.id) {
            lastSelected.material = lastSelected.userData.orig_material;
          }

          intersectObj.material = new THREE.MeshBasicMaterial({
            color: 0x18BC9C,
            wireframe: true,
          });
          lastSelected = intersectObj;
        }
      }

      riot.shaft.on('selection-changed', () => {
        if (riot.shaft.selected !== undefined) {
          let selectedObj;
          if (riot.shaft.selected.nodes !== undefined && riot.shaft.selected.nodes.length > 0) {
            selectedObj = valGroup.getObjectByName(riot.shaft.selected.nodes[0].id);
          } else {
            selectedObj = valGroup.getObjectByName(riot.shaft.selected.id);
          }

          if (lastSelected && selectedObj.name !== lastSelected.name) {
            lastSelected.material = lastSelected.userData.orig_material;
          }

          selectedObj.material = new THREE.MeshBasicMaterial({
            color: 0x18BC9C,
            wireframe: true,
          });
          lastSelected = selectedObj;
        } else {
          lastSelected.material = lastSelected.userData.orig_material;
        }
      });

      function saveFile(strData, filename) {
        const link = document.createElement('a');
        if (typeof link.download === 'string') {
          document.body.appendChild(link);
          link.download = filename;
          link.href = strData;
          link.click();
          document.body.removeChild(link);
        } else {
          window.location.replace(window.uri);
        }
      }

      function saveAsImage() {
        let imgData;
        try {
          const strMime = 'image/png';
          imgData = renderer.domElement.toDataURL(strMime);
          saveFile(imgData.replace(strMime, strDownloadMime), 'test.png');
        } catch (e) {
          console.log(e);
        }
      }

      function render() {
        renderer.render(scene, camera);
        renderer2.render(scene2, camera2);
      }

      function animate() {
        requestAnimationFrame(animate);
        camera2.position.copy(camera.position);
        camera2.position.sub(controls.target); // added by @libe
        camera2.position.setLength(10);

        light.position.copy(camera.position);
        camera2.lookAt(scene2.position);
        render();
      }

      function getRandomArbitrary(min, max) {
        return (Math.random() * (max - min)) + min;
      }

      function cameraOZ() {
        controls.reset();
        camera.position.x = 0;
        camera.position.y = 0;
        camera.position.z = 100;
        camera.lookAt(0, 0, 0);
        controls.update();
      }

      function cameraOX() {
        controls.reset();
        camera.position.x = 100;
        camera.position.y = 0;
        camera.position.z = 0;
        camera.lookAt(0, 0, 0);
        controls.update();
      }

      function draw() {
        $.each(riot.shaft.parts, (index, part) => {
          if (part.type === 'shaft') {
            const el = getValElement(
              part.nodes[0].render_params.d1,
              part.nodes[0].render_params.d2,
              part.nodes[0].render_params.d1_1,
              part.nodes[0].render_params.d2_1,
              part.render_params.length,
              part.nodes[0].render_params.color,
            );
            el.position.x += elementOffset + (part.render_params.length / 2);
            el.name = part.nodes[0].id;
            valGroup.add(el);

            if (part.render_params.layer_count >= 2) {
              const e2 = getValElement(
                part.nodes[0].render_params.d1_1,
                part.nodes[0].render_params.d2_1,
                part.nodes[1].render_params.d1_1,
                part.nodes[1].render_params.d2_1,
                part.render_params.length,
                part.nodes[1].render_params.color,
              );
              e2.position.x += elementOffset + (part.render_params.length / 2);
              e2.name = part.nodes[1].id;
              valGroup.add(e2);
            }
            if (part.render_params.layer_count === 3) {
              const e3 = getValElement(
                part.nodes[1].render_params.d1_1,
                part.nodes[1].render_params.d2_1,
                part.nodes[2].render_params.d1_1,
                part.nodes[2].render_params.d2_1,
                part.render_params.length,
                part.nodes[2].render_params.color,
              );
              e3.position.x += elementOffset + (part.render_params.length / 2);
              e3.name = part.nodes[2].id;
              valGroup.add(e3);
            }

            elementOffset += part.render_params.length;
          } else if (part.type === 'bear') {
            const el = getBearingK(
              part.id,
              part.render_params.p1,
              part.render_params.p2,
              part.render_params.p3,
              part.render_params.p4,
              part.render_params.p5,
              part.render_params.p6,
            );
            el.position.x += elementOffset + (part.render_params.p3 / 2);
            elementOffset += part.render_params.p3;
            // el.name = part.text;
            valGroup.add(el);
          }
        });
        animate();
        // var el = getValElement(2, 3, 5, 3, 10);
        // el.position.x -= 35;
        // valGroup.add(el);
        //
        // var el1 = getValElement(2, 2, 3, 3, 20);
        // el1.position.x -= 20;
        // valGroup.add(el1);
        //
        // var fln = getFlange(15, 1, 10, 2, 7, 3, 5, 4);
        // fln.position.x -= 20;
        // valGroup.add(fln);
        //
        // var muf = getMuff(3, 5, 8, 20, 10);
        // valGroup.add(muf);
        //
        // var el2 = getValElement(3, 3, 5, 5, 20);
        // el2.position.x += 20;
        // valGroup.add(el2);
        //
        // var bearing = getBearingK(5, 3, 20, 4, 1, 8);
        // bearing.position.x += 20;
        // valGroup.add(bearing);
        //
        // var gear2 = getGear(0.02, 0.01);
        // gear2.position.x -= 35;
        // valGroup.add(gear2);
        //
        // var prop = getPropeller(0, 0);
        // prop.position.x += 20;
        // valGroup.add(prop);
      }


      init();
      miniAxis();

      loadModels();
      Promise.all(allPromises)
        .then(() => {
          draw();
          riot.shaft.trigger('loading-finished');
        }, () => {
          console.error('Error while loading models');
        });

      window.addEventListener('click', onClick, false);
      document.getElementById('btn_scrn').addEventListener('click', saveAsImage);
      document.getElementById('btn_z').addEventListener('click', cameraOZ);
      document.getElementById('btn_x').addEventListener('click', cameraOX);

      function clearScene() {
        const toRemove = [];
        valGroup.traverse((child) => {
          if (child instanceof THREE.Object3D) {
            toRemove.push(child);
          }
        });

        for (let i = 0; i < toRemove.length; i += 1) {
          valGroup.remove(toRemove[i]);
        }
      }

      riot.shaft.on('part-changed', () => {
        clearScene();
        elementOffset = 0;
        draw();
      });
    });
  </script>
</three>