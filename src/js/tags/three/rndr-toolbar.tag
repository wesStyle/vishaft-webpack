<!--
Тулбар для кнопок управления рендером
-->

<rndr-toolbar>
  <div id="rndr-toolbar">
    <button type="button" id="btn_x" class="btn btn-secondary">
      <i class="fas fa-arrow-left"></i>
    </button>
    <button type="button" id="btn_z" class="btn btn-secondary">
      <i class="fas fa-arrow-up"></i>
    </button>
    <button type="button" id="btn_scrn" class="btn btn-info">
      <i class="fas fa-image"></i>
    </button>
  </div>
</rndr-toolbar>