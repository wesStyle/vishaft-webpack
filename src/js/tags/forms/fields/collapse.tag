<!--
Разворачивающийся список

Параметры:
ref - свойства href и id для работы кнопки
label - текст кнопки a
-->

<collapse>
  <p>
    <a class="btn btn-outline-primary btn-block" data-toggle="collapse" href="#{ opts.ref }" role="button" aria-expanded="false" aria-controls="collapseExample">
      { opts.label }
    </a>
  </p>
  <div class="{ collapse: !opts.is_opened,  show: opts.is_opened}" id="{ opts.ref }">
    <div class="card card-body">
      <yield/>
    </div>
  </div>
</collapse>