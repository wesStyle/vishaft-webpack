<!--
Выпадающий список на основе select

Параметры:
list - массив значений выпадающего списка вида [ {name, val} ]
val - выбранный элемент ref - свойства id, ref в input
description - текст описания в small label - текст названия поля в label
clazz - добавочный класс для select
-->

<select-dropdown>
  <div>
    <div class="form-group">
      <label for="{ opts.ref }">{ opts.label }</label>
      <select class="form-control { opts.clazz }" ref="{ opts.ref }" name="{ opts.ref }" id="{ opts.ref }">
        <option each="{ opts.list }" value="{ val }" selected="{opts.val == val}">{ name }</option>
      </select>
      <small id="{ opts.ref }Help" class="form-text text-muted" if="{ opts.description }">{ opts.description }</small>
    </div>
  </div>

  <script>
  </script>
</select-dropdown>