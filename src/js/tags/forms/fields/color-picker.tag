<!--
Выпадающий список на основе <select>

Параметры:
list - список предварительно заданных цветов
ref - свойства id, ref в input
description - текст описания в small
label - текст названия поля в label
clazz - добавочный класс для input
-->

<color-picker>
  <div class="form-group">
    <label for="{ opts.ref }">{ opts.label }</label>

    <div ref="picker" class="input-group mb-3 colorpicker-component">
      <input type="text" class="form-control input-lg { opts.clazz }" ref="{ opts.ref }" id="{ opts.ref }" name="{ opts.ref }" value="{ opts.val }">
      <div class="input-group-append">
        <span class="input-group-text input-group-addon">
          <i></i>
        </span>
      </div>
      <small id="{ opts.ref }Help" class="form-text text-muted" if="{ opts.description }">{ opts.description }</small>
    </div>

    <script>
      import 'bootstrap-colorpicker';
      import 'bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css';

      const tag = this;

      tag.on('mount', () => {
        $(tag.refs.picker).colorpicker({
          useAlpha: false,
          extensions: [
            {
              name: 'swatches',
              colors: opts.list,
              namesAsValues: false,
            },
          ],
        });
      });
    </script>
</color-picker>