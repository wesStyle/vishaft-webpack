<!-- 
Поля для ввода

Параметры:
ref - свойства id, ref в input
type - свойство type в input
placeholder - свойство placeholder в input
val - свойство value в input
description - текст описания в small
label - текст названия поля в label
clazz - добавочный класс для input
-->

<input-field>
  <div class="form-group">
    <label for="{ opts.ref }">{ opts.label }</label>
    <input type="{ opts.type }" class="form-control { opts.clazz }" ref="{ opts.ref }" id="{ opts.ref }" name="{ opts.ref }" placeholder="{ opts.placeholder }"
      value="{ opts.val }">
    <small id="{ opts.ref }Help" class="form-text text-muted" if="{ opts.description }">{ opts.description }</small>
  </div>

  <script>
  </script>
</input-field>