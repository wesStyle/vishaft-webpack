<!--
Выбор подходящей формы и кнопки Сохранить/Добавить

Параметры:
element_type - строка с типом элемента
is_new - является ли форма формой добавления нового элемента: true/false
-->

<form-selector>
  <div>
    <bearing-type1-add if="{ opts.element_type=='bear' && opts.is_new }" ref="{ opts.element_type }" clazz="{ clazz }" />
    <bearing-type1-edit if="{ opts.element_type=='bear' && !opts.is_new }" ref="{ opts.element_type }" clazz="{ clazz }" />

    <!-- Вал -->
    <shaft-add if="{ opts.element_type=='shaft' && opts.is_new }" ref="{ opts.element_type }" clazz="{ clazz }" />
    <shaft-edit if="{ opts.element_type=='shaft' && !opts.is_new }" ref="{ opts.element_type }" clazz="{ clazz }" />

    <!-- Вал слой -->
    <shaft-layer-edit if="{ opts.element_type=='shaft-layer1' && !opts.is_new }" ref="{ opts.element_type }" clazz="{ clazz }"
    />
    <shaft-layer-edit if="{ opts.element_type=='shaft-layer2' && !opts.is_new }" ref="{ opts.element_type }" clazz="{ clazz }"
      is_sub_layer={ true }/>
    <shaft-layer-edit if="{ opts.element_type=='shaft-layer3' && !opts.is_new }" ref="{ opts.element_type }" clazz="{ clazz }"
      is_sub_layer={ true }/>
    <div ref="empty_box" if="{ !opts.element_type }">
      <div class="alert alert-secondary text-center" role="alert">
          Выберите элемент
        </div>
    </div>
  </div>
  <div class="alert alert-success alert-dismissible fade show" role="alert" ref="save_alert">
    Изменения сохранены
    <button type="button" class="close" ref="btn_close_alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div id='bottom-btn'>
    <div class="btn btn-primary btn-block" ref="btn_save">Сохранить изменения</div>
    <div class="btn btn-primary btn-block" ref="btn_add">Добавить элемент</div>
  </div>

  <script>
    import Sugar from 'sugar';

    import './model/shaft/shaft-add.tag';
    import './model/shaft/shaft-edit.tag';
    import './model/shaft/layer/shaft-layer-edit.tag';

    const tag = this;

    if (opts.is_new) {
      tag.clazz = 'element-add-field';
    } else {
      tag.clazz = 'element-edit-field';
    }

    this.on('mount', () => {
      $(tag.refs.btn_close_alert).click(() => {
        $(tag.refs.save_alert).hide();
      });

      $(tag.refs.btn_save).click(() => {
        if (tag.refs[opts.element_type].validate()) {
          $(`.${tag.clazz}`).each((i, el) => {
            if (el.id === 'txt') {
              riot.shaft.selected.text = $(el).val();
            } else if (el.type === 'number') {
              riot.shaft.selected.render_params[el.id] = parseInt($(el).val(), 10);
            } else {
              riot.shaft.selected.render_params[el.id] = $(el).val();
            }
          });

          riot.shaft.trigger('part-changed');
          // riot.shaft.selected = undefined;
          riot.shaft.trigger('selection-changed');
          tag.update({ save_success: true });
        }
      });

      $(tag.refs.btn_add).click(() => {
        const { newElement } = tag.refs[opts.element_type];

        $(`.${tag.clazz}`).each((i, el) => {
          if (el.id === 'txt') {
            newElement.text = $(el).val();
          } else if (el.type === 'number') {
            newElement.render_params[el.id] = parseInt($(el).val(), 10);
          } else {
            newElement.render_params[el.id] = $(el).val();
          }
        });

        if (riot.shaft.selected !== undefined) {
          const el = riot.shaft.selected.parent ? riot.shaft.selected.parent : riot.shaft.selected;
          const pos = riot.shaft.parts.map(e => e.id).indexOf(el.id);

          Sugar.Array.insert(riot.shaft.parts, newElement, pos + 1);
        } else {
          riot.shaft.parts.push(newElement);
        }
        riot.shaft.trigger('part-changed');
        riot.shaft.selected = newElement;
        riot.shaft.trigger('selection-changed');
      });

      $(tag.refs.btn_save).hide();
      $(tag.refs.btn_add).hide();
      $(tag.refs.save_alert).hide();
    });

    this.on('update', () => {
      $(tag.refs.btn_save).hide();
      $(tag.refs.btn_add).hide();
      $(tag.refs.save_alert).hide();
      if (tag.save_success) {
        $(tag.refs.save_alert).fadeTo(2000, 500).slideUp(500, () => {
          $(tag.refs.save_alert).slideUp(500);
        });
        tag.save_success = false;
      }

      if (opts.element_type && !opts.is_new) {
        $(tag.refs.btn_save).show();
      } else if (opts.element_type && opts.is_new) {
        $(tag.refs.btn_add).show();
        $(tag.refs.empty_box).hide();
      }
    });
  </script>
</form-selector>