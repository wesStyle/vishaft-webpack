export default {
  materials: [
    { name: '[элем] Бронза', val: 1 },
    { name: '[элем] Сталь', val: 2 },
    { name: '[элем] Титан', val: 3 },
  ],
  envMaterials: [
    { name: '[сред] Воздух', val: 1 },
    { name: '[сред] Вода', val: 2 },
  ],
};
