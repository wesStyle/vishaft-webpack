<!--
  Элемент Валопровода: Вал

  Форма добавления элемента
-->

<shaft-add>
  <div>
    <form>
      <input-field ref="txt" label="Имя" val="{ newElement.text }" type="text" clazz="{ clazz }" />
      <input-field ref="length" label="Длина" val="{ newElement.render_params.length }" type="number" clazz="{ clazz }" />
    </form>
  </div>
  <script>
    import ShaftShaft from './shaft';
    import '../../fields/input-field.tag';

    const tag = this;

    tag.clazz = opts.clazz; // Класс для элементов ввода на форме
    tag.newElement = new ShaftShaft();

    tag.on('update', () => {
      tag.newElement = new ShaftShaft();
    });
  </script>
</shaft-add>