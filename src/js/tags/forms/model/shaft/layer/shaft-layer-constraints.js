/*
Элемент Валопровода: Вал слой

Лог. проверки полей
*/

export default {
  transparency: {
    presence: true,
    numericality: {
      onlyInteger: true,
      greaterThanOrEqualTo: 0,
      lessThanOrEqualTo: 100,
    },
  },
};
