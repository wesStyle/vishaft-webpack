<!--
Элемент Валопровода: Вал слой

Форма редактирования элемента

Параметры:
is_sub_layer - является ли слой не первым - true/false
-->

<shaft-layer-edit>
  <div>
    <form ref="editForm">
      <collapse label="Misc" ref="group1" is_opened="{true}">
        <select-dropdown if="{ !parent.is_sub }" ref="is_hole" label="Наличие полости" val="{ parent.selected.render_params.is_hole }"
          list="{ [{name: 'Да', val: 1}, {name: 'Нет', val: 0}] }" clazz="{ parent.clazz }" />
        <select-dropdown ref="material" label="Материал" val="{ parent.selected.render_params.material }" list="{ parent.shaftMaterials }"
          clazz="{ parent.clazz }" />
        <color-picker ref="color" label="Цвет" val="{ parent.selected.render_params.color }" list="{ parent.colors }" clazz="{ parent.clazz }"
        />
        <input-field ref="transparency" label="Прозрачность" val="{ parent.selected.render_params.transparency }" type="number" clazz="{ parent.clazz }"
        />
      </collapse>
      <collapse label="Размеры" ref="group2">
        <input-field ref="d1" if="{ !parent.is_sub }" label="D полости кормовой грани" val="{ parent.selected.render_params.d1 }"
          type="number" clazz="{ parent.clazz }" />
        <input-field ref="d2" if="{ !parent.is_sub }" label="D полости носовой грани" val="{ parent.selected.render_params.d2 }"
          type="number" clazz="{ parent.clazz }" />
        <input-field ref="d1_1" label="D кормовой грани" val="{ parent.selected.render_params.d1_1 }" type="number" clazz="{ parent.clazz }"
        />
        <input-field ref="d2_1" label="D носовой грани" val="{ parent.selected.render_params.d2_1 }" type="number" clazz="{ parent.clazz }"
        />
      </collapse>
    </form>
    <div ref="alert-box">
        <div each="{ msg, i in errMsgs }" class="alert alert-warning" role="alert">
          { msg }
        </div>
    </div>
  </div>

  <script>
    import validate from 'validate.js';

    import '../../../fields/collapse.tag';
    import '../../../fields/color-picker.tag';
    import '../../../fields/input-field.tag';
    import '../../../fields/select-dropdown.tag';

    import colors from '../../colors';
    import mats from '../../materials';

    import constraints from './shaft-layer-constraints';

    const tag = this;

    tag.colors = colors;
    tag.shaftMaterials = mats.materials;

    tag.selected = riot.shaft.selected;
    tag.clazz = opts.clazz; // Класс для элементов ввода на форме
    tag.is_sub = opts.is_sub_layer;

    tag.on('update', () => {
      tag.errMsgs = [];
      $(`.${tag.clazz}`).removeClass('is-invalid');
      $.each(tag.errs, (id, err) => {
        $(`#${id}.${tag.clazz}`).addClass('is-invalid');
        tag.errMsgs = tag.errMsgs.concat(err);
      });
    });

    tag.validate = () => {
      tag.errs = validate.validate(tag.refs.editForm, constraints);
      tag.update();
      if (tag.errs) {
        return false;
      }
      return true;
    };
  </script>
</shaft-layer-edit>