<!--
Элемент Валопровода: Вал

Форма редактирования элемента
-->

<shaft-edit>
  <div>
    <form>
      <collapse label="Misc" ref="group1" is_opened="{true}">
        <input-field ref="max_tension" label="Макс. допустимое напряжение" val="{ parent.selected.render_params.max_tension }" type="number"
          description="Значение максимально допустимого напряжения в вале (МПа)" clazz="{ parent.clazz }" />
        <input-field ref="layer_count" label="Кол-во слоев" val="{ parent.selected.render_params.layer_count }" type="number" description="Определяет количество слоев элементов вала"
          clazz="{ parent.clazz }" />
        <input-field ref="txt" label="Имя" val="{ parent.selected.text }" type="text" clazz="{ parent.clazz }" />
      </collapse>
      <collapse label="Материалы" ref="group2">
        <select-dropdown ref="env_material" label="Материал среды" val="{ parent.selected.render_params.env_material }" list="{ parent.shaftMaterials
          }" clazz="{ parent.clazz }" />
      </collapse>
      <collapse label="Размеры" ref="group3">
        <input-field ref="main_girder_size" label="Осевой размер балочных конечных элементов" val="{ parent.selected.render_params.main_girder_size }"
          type="number" description="Величина размера вводится в миллиметрах. Осевой размер балочных элементов может быть меньше или равен длине вала"
          clazz="{ parent.clazz }" />
        <input-field ref="length" label="Длина" val="{ parent.selected.render_params.length }" type="number" clazz="{ parent.clazz }"
        />
      </collapse>
      <collapse label="Расположение" ref="group4">
      </collapse>
    </form>
  </div>

  <script>
    import '../../fields/collapse.tag';
    import '../../fields/color-picker.tag';
    import '../../fields/input-field.tag';
    import '../../fields/select-dropdown.tag';

    import mats from '../materials';

    const tag = this;
    tag.shaftMaterials = mats.materials;

    tag.selected = riot.shaft.selected;
    tag.clazz = opts.clazz; // Класс для элементов ввода на форме

    tag.validate = () => true;
  </script>
</shaft-edit>