/*
Элемент Валопровода: Вал

Значения свойств по-умолчанию для нового экземпляра элемента
*/

import generateID from '../../../../helper';

export default class ShaftShaft {
  constructor() {
    const shaft = this;

    this.text = 'Вал 0';
    this.type = 'shaft';
    this.icon = 'fas fa-circle fa-fw jui';
    this.id = generateID.generate(12);

    this.render_params = {
      max_tension: 0,
      layer_count: 3,
      env_material: 1,
      main_girder_size: 0,
      length: 20,
    };

    this.nodes = [
      {
        parent: shaft,
        id: generateID.generate(12),
        text: 'Слой 1',
        type: 'shaft-layer1',
        icon: 'fas fa-dot-circle fa-fw jui',
        render_params: {
          is_hole: 0,
          material: 3,
          color: '#777777',
          transparency: 0,
          d1: 5,
          d2: 5,
          d1_1: 10,
          d2_1: 10,
        },
      },
      {
        parent: shaft,
        id: generateID.generate(12),
        text: 'Слой 2',
        type: 'shaft-layer2',
        icon: 'fas fa-dot-circle fa-fw jui',
        render_params: {
          material: 3,
          color: '#337ab7',
          transparency: 0,
          d1_1: 20,
          d2_1: 20,
        },
      },
      {
        parent: shaft,
        id: generateID.generate(12),
        text: 'Слой 3',
        type: 'shaft-layer3',
        icon: 'fas fa-dot-circle fa-fw jui',
        render_params: {
          material: 3,
          color: '#d9534f',
          transparency: 0,
          d1_1: 30,
          d2_1: 30,
        },
      },
    ];
  }
}
