<!--
Область показа свойств выбранного элемента

Параметры:
element_type - строка с типом элемента
-->

<prop>
  <div>
    <form-selector element_type="{ element_type }"></form-selector>
  </div>

  <script>
    import './forms/form-selector.tag';

    const tag = this;

    riot.shaft.on('selection-changed', () => {
      if (riot.shaft.selected !== undefined) {
        tag.element_type = riot.shaft.selected.type;
      } else {
        tag.element_type = undefined;
      }
      tag.update();
    });

  </script>
</prop>