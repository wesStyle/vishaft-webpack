<!--
Дерево элементов
-->

<tree>
  <div id="tree">
  </div>

  <script>
    import 'bootstrap-treeview/dist/bootstrap-treeview.min';
    import 'bootstrap-treeview/dist/bootstrap-treeview.min.css';

    const tag = this;

    function loadTree() {
      const nodes = riot.shaft.parts.length === 0 ? null : riot.shaft.parts;

      const treeData = [{
        text: riot.shaft.text,
        nodes,
      }];

      $('#tree').treeview({
        data: treeData,
        levels: 3,
        onhoverColor: '#d4d8d8',
        selectedBackColor: '#7b8a8b',
        searchResultColor: '#ffffff',
        collapseIcon: 'fas fa-minus-square fa-fw',
        expandIcon: 'fas fa-plus-square fa-fw',
      });

      $('#tree').on('nodeSelected', (event, data) => {
        $.each(riot.shaft.parts, (index, value) => {
          if (value.nodes !== undefined) {
            $.each(value.nodes, (index1, value1) => {
              if (value1.id === data.id) {
                riot.shaft.selected = value1;
                riot.shaft.trigger('selection-changed');
              }
            });
          }
          if (value.id === data.id) {
            riot.shaft.selected = value;
            riot.shaft.trigger('selection-changed');
          }
        });
      });

      $('#tree').on('nodeUnselected', () => {
        riot.shaft.selected = undefined;
        riot.shaft.trigger('selection-changed');
      });
    }

    tag.on('mount', () => {
      loadTree();
    });

    riot.shaft.on('part-changed', () => {
      $('#tree').treeview('remove');
      loadTree();
    });

    riot.shaft.on('selection-changed', () => {
      if (riot.shaft.selected !== undefined) {
        $.each($('#tree').treeview('getUnselected'), (index, value) => {
          if (value.nodes !== undefined) {
            $.each(value.nodes, (index1, value1) => {
              if (value1.id === riot.shaft.selected.id) {
                $('#tree').treeview('selectNode', [value1.nodeId]);
              }
            });
          }
          if (value.id === riot.shaft.selected.id) {
            $('#tree').treeview('selectNode', [value.nodeId]);
          }
        });
      }
    });

  </script>
</tree>