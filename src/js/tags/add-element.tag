<add-element>
  <div class="btn-group btn-group-justified" role="group" aria-label="Button group with nested dropdown">
    <div class="btn-group" role="group">
      <button id="btnGroupDrop1" type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false">
        <i class="fas fa-plus-circle"></i> Добавить
      </button>
      <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModal" onclick="{ open_form.bind(this, 'shaft') }">Вал</a>
      </div>
    </div>
    <a role="button" ref="btn_delete" class="btn btn-danger" href="#">
      <i class="fas fa-minus-circle"></i> Удалить</a>
  </div>

  <div class="modal show" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"> Добавить</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form-selector element_type={ element_type } is_new={ true }/>
        </div>
      </div>
    </div>
  </div>

  <script>
    /*
      Кнопка добавления/удаления элемента и вызов модального окна добавления элемента
    */
    import Sugar from 'sugar';
    import './forms/form-selector.tag';

    const tag = this;

    tag.open_form = (e) => {
      tag.element_type = e;
    };

    this.on('mount', () => {
      $(tag.refs.btn_delete).click(() => {
        if (riot.shaft.selected !== undefined) {
          let toRemove = riot.shaft.selected;
          if (toRemove.parent) {
            toRemove = toRemove.parent;
          }

          Sugar.Array.remove(riot.shaft.parts, el => el.id === toRemove.id);
          riot.shaft.trigger('part-changed');
          riot.shaft.selected = undefined;
          riot.shaft.trigger('selection-changed');
        }
      });
    });
    riot.shaft.on('part-changed', () => {
      $('#exampleModal').modal('hide');
    });
  </script>
</add-element>