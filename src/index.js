import 'riot-hot-reload';

import 'bootstrap';
import '@fortawesome/fontawesome';
import '@fortawesome/fontawesome-free-regular';
import '@fortawesome/fontawesome-free-solid';

import './js/app.tag';
import './css/bootstrap.min.css';
import './css/custom.css';

window.onbeforeunload = () => 'Все несохраненные данные будут утеряны!';

riot.mount('app');
